/*
 * JoyBusProtocol.h
 *
 *  Created on: Jan 31, 2015
 *      Author: laurencedv
 */

#ifndef JOYBUSPROTOCOL_H_
#define JOYBUSPROTOCOL_H_

/* -------------------------------- +
|	Include							|
+ ---------------------------------*/
#include <msp430.h>

/* -------------------------------- +
|	Define							|
+ ---------------------------------*/
// ==== Timing ==== //
#define CMD_MAX_SIZE			(8*8)			//largest command in bit
#define TIMER_CLOCK				25000000
#define JOYBUS_BIT_PERIOD_US	4				//bit period in �s
#define JOYBUS_BIT_LOW_DUTY		20				//in percent
#define JOYBUS_BIT_HIGH_DUTY	80				//in percent

#define JOYBUS_BIT_LOW_IN_THREASHOLD	5
// ================ //

// ==== Commands ==== //
#define CMD_OPCODE_INIT					0x00
#define CMD_SIZE_INIT					(1*8)
#define CMD_RESPONSE_SIZE_INIT			(3*8)

#define CMD_OPCODE_POLL					0x400302
#define CMD_SIZE_POLL					(3*8)
#define CMD_RESPONSE_SIZE_POLL			(8*8)

#define CMD_OPCODE_RUMBLE_ON			0x00000001
#define CMD_SIZE_RUMBLE_ON				(3*8)
#define CMD_RESPONSE_SIZE_RUMBLE_ON		0

#define CMD_OPCODE_RUMBLE_OFF			0x00000000
#define CMD_SIZE_RUMBLE_OFF				(3*8)
#define CMD_RESPONSE_SIZE_RUMBLE_OFF	0
// ================== //


// ==== Response ==== //
// Init response
#define GAMEPAD_TYPE_N64			0x0500
#define GAMEPAD_TYPE_N64_MIC		0x0001
#define GAMEPAD_TYPE_N64_KEYBOARD	0x0002
#define GAMEPAD_TYPE_N64_MOUSE		0x0200
#define GAMEPAD_TYPE_GBA			0x0004
#define GAMEPAD_TYPE_GBA2			0x0800		//Same as GC wheel
#define GAMEPAD_TYPE_GC				0x0900		//Same as DKongas
#define GAMEPAD_TYPE_GC_KEYBOARD	0x0820
#define GAMEPAD_TYPE_GC_WAVEBIRD	0xE960		//could be 0xE9A0, 0xA800, 0xEBB0
#define GAMEPAD_TYPE_GC_WHEEL		0x0800		//Same as GBA2
#define GAMEPAD_TYPE_DKONGAS		0x0900		//Same as GC
#define GAMEPAD_STATUS_RUMBLE_ON	0x08
#define GAMEPAD_STATUS_RUMBLE_OFF	0x00

typedef union{
	struct {
		uint8_t		gamePadStatus:8;
		uint16_t	gamePadType:16;
	};
	uint32_t		all;
}cmdInitResponse_t;

typedef union {
	struct {
		uint8_t		triggerRight;
		uint8_t		triggerLeft;
		uint8_t		cAxisY;
		uint8_t 	cAxisX;
		uint8_t 	joyAxisY;
		uint8_t 	joyAxisX;
		uint8_t		dPadLeft:1;
		uint8_t		dPadRight:1;
		uint8_t		dPadDown:1;
		uint8_t		dPadUp:1;
		uint8_t		btnZ:1;
		uint8_t		btnTriggerRight:1;
		uint8_t		btnTriggerLeft:1;
		uint8_t		:1;
		uint8_t		btnA:1;
		uint8_t		btnB:1;
		uint8_t		btnX:1;
		uint8_t		btnY:1;
		uint8_t		btnStart:1;
		uint8_t		:1;
		uint8_t		errorLatch:1;
		uint8_t		errorStatus:1;
	};
	uint64_t		all;
}cmdPollResponse_t;

typedef enum {
	disconnected,
	probing,
	connected,
	polling,
	answering,
	parsing,
	reporting
}gpState_t;

typedef enum {
	cmdInit,
	cmdPoll,
	cmdRumble_on,
	cmdRumble_off
}cmdType_t;
// ================== //

// ==== Internal ==== //
#define __TIMER_TICK_NS			(1000000000/TIMER_CLOCK)

#define __JOYBUS_BIT_PERIOD		(((JOYBUS_BIT_PERIOD_US*1000)/__TIMER_TICK_NS)-1)			//bit period in timer count (JOYBUS_BIT_PERIOD / (1/TimerClock)) - 1
#define __JOYBUS_BIT_HIGH		((JOYBUS_BIT_LOW_DUTY*__JOYBUS_BIT_PERIOD)/100)
#define __JOYBUS_BIT_LOW		((JOYBUS_BIT_HIGH_DUTY*__JOYBUS_BIT_PERIOD)/100)
// ================== //


/* -------------------------------- +
|	Global Variable					|
+ ---------------------------------*/
const uint16_t outputBuffInit[10] = {	0,
										__JOYBUS_BIT_HIGH,

										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,

										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW};

const uint16_t outputBuffPoll[26] = {	0,
										__JOYBUS_BIT_HIGH,

										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_HIGH,
										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,

										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,

										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,

										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_HIGH,
										__JOYBUS_BIT_HIGH,

										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,

										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_LOW,
										__JOYBUS_BIT_HIGH,
										__JOYBUS_BIT_LOW};

#endif
