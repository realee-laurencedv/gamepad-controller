/* --COPYRIGHT--,BSD
 * Copyright (c) 2014, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
/*
 * ======== hal.c ========
 *
 */
#include "msp430.h"

#include "driverlib.h"

#include "hal.h"

#define GPIO_ALL	GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3| \
					GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7



/*
* This function drives all the I/O's as output-low, to avoid floating inputs
* (which cause extra power to be consumed).  This setting is compatible with  
 * TI FET target boards, the F5529 Launchpad, and F5529 Experimenters Board;  
 * but may not be compatible with custom hardware, which may have components  
 * attached to the I/Os that could be affected by these settings.  So if using
* other boards, this function may need to be modified.
*/
void initPorts(void)
{
	#ifdef __MSP430_HAS_PORT1_R__
		GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_ALL);
		GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_ALL);

		//GPIO_setDriveStrength(GPIO_PORT_P1, GPIO_PIN2, GPIO_FULL_OUTPUT_DRIVE_STRENGTH);
		GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN5 + GPIO_PIN4 + GPIO_PIN3 + GPIO_PIN2);

		//GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P1, GPIO_PIN1);
		//GPIO_enableInterrupt(GPIO_PORT_P1, GPIO_PIN1);
		//GPIO_interruptEdgeSelect(GPIO_PORT_P1, GPIO_PIN1, GPIO_HIGH_TO_LOW_TRANSITION);
		//GPIO_clearInterruptFlag(GPIO_PORT_P1, GPIO_PIN1);
	#endif

	#ifdef __MSP430_HAS_PORT2_R__
		GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_ALL);
		GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_ALL);
		GPIO_setAsInputPin(GPIO_PORT_P2, GPIO_PIN5);

		GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P2, GPIO_PIN0 + GPIO_PIN2);
		//GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P2, GPIO_PIN5);

		//GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P2, GPIO_PIN1);
		//GPIO_enableInterrupt(GPIO_PORT_P2, GPIO_PIN1);
		//GPIO_interruptEdgeSelect(GPIO_PORT_P2, GPIO_PIN1, GPIO_HIGH_TO_LOW_TRANSITION);
		//GPIO_clearInterruptFlag(GPIO_PORT_P2, GPIO_PIN1);
	#endif

	#ifdef __MSP430_HAS_PORT3_R__
		GPIO_setOutputLowOnPin(GPIO_PORT_P3, GPIO_ALL);
		GPIO_setAsOutputPin(GPIO_PORT_P3, GPIO_ALL);
	#endif

	#ifdef __MSP430_HAS_PORT4_R__
		GPIO_setOutputLowOnPin(GPIO_PORT_P4, GPIO_ALL);
		GPIO_setAsOutputPin(GPIO_PORT_P4, GPIO_ALL);
	#endif

	#ifdef __MSP430_HAS_PORT5_R__
		GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_ALL);
		GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_ALL);

		GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P5, GPIO_PIN2);
		GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P5, GPIO_PIN3);
	#endif

	#ifdef __MSP430_HAS_PORT6_R__
		GPIO_setOutputLowOnPin(GPIO_PORT_P6, GPIO_ALL);
		GPIO_setAsOutputPin(GPIO_PORT_P6, GPIO_ALL);
	#endif

	#ifdef __MSP430_HAS_PORT7_R__
		GPIO_setOutputLowOnPin(GPIO_PORT_P7, GPIO_ALL);
		GPIO_setAsOutputPin(GPIO_PORT_P7, GPIO_ALL);
	#endif

	#ifdef __MSP430_HAS_PORT8_R__
		GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_ALL);
		GPIO_setAsOutputPin(GPIO_PORT_P8, GPIO_ALL);
	#endif

	#ifdef __MSP430_HAS_PORT9_R__
		GPIO_setOutputLowOnPin(GPIO_PORT_P9, GPIO_ALL);
		GPIO_setAsOutputPin(GPIO_PORT_P9, GPIO_ALL);
	#endif

	#ifdef __MSP430_HAS_PORTJ_R__
		GPIO_setOutputLowOnPin(GPIO_PORT_PJ, GPIO_ALL);
		GPIO_setAsOutputPin(GPIO_PORT_PJ, GPIO_ALL);
	#endif
}

/* Configures the system clocks:
* MCLK = SMCLK = DCO/FLL = mclkFreq (expected to be expressed in Hz)
* ACLK = FLLref = REFO=32kHz
*
*/
uint32_t initClocks(uint32_t mclkFreq)
{
	if(!PMM_setVCore(PMM_CORE_LEVEL_3))									//Drive to maximum Vcore for 25MHz operation
		__no_operation();												//STATUS_SUCCESS = 1 in TI driverlib

	UCS_setExternalClockSource(XTAL_FREQ_XT1_HZ,XTAL_FREQ_XT2_HZ);		//Tell the system what is the freq of the crystals

	if (UCS_XT2StartWithTimeout(UCS_XT2DRIVE_4MHZ_8MHZ, 1000))
		__no_operation();
	UCS_clockSignalInit(UCS_FLLREF, UCS_XT2CLK_SELECT, UCS_CLOCK_DIVIDER_16);
	UCS_clockSignalInit(UCS_ACLK, UCS_REFOCLK_SELECT, UCS_CLOCK_DIVIDER_1);
	if (UCS_clearAllOscFlagsWithTimeout(1000))
		__no_operation();
    UCS_initFLLSettle(mclkFreq/1000, mclkFreq/250000);

    UCS_clockSignalInit(UCS_SMCLK, UCS_DCOCLK_SELECT, UCS_CLOCK_DIVIDER_1);

    return UCS_getMCLK();
}
