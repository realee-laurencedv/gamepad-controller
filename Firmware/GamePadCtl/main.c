// Header bullshit here


/* -------------------------------- +
|	Include							|
+ ---------------------------------*/
#include "driverlib.h"
#include <msp430.h>
#include "hal.h"
#include "JoyBusProtocol.h"

/* -------------------------------- +
|	Define							|
+ ---------------------------------*/
#define SYSTEM_CLOCK		25000000

#define DBG_LOW_PORT		(P6OUT)
#define DBGPRINT(dbgCode)	DBG_LOW_PORT = dbgCode

#define JOYBUS_MASKED_PIN	(P2IN&GPIO_PIN5)

#define STACK_OVERFLOW_CHECK_VAL	0x22

/* -------------------------------- +
|	Global Variable					|
+ ---------------------------------*/
uint16_t stackOverflowCheck = STACK_OVERFLOW_CHECK_VAL;

// Output subsystem
const uint16_t * outputBuffPtr;
uint16_t outputBuffNext;

const uint16_t * outputLimitPtr;

//Input subsystem
uint16_t inputBuffer[CMD_MAX_SIZE+2];
uint16_t * inputBuffPtr = &inputBuffer[0];
uint16_t * inputLimitPtr;
uint16_t inputAccu = 0;

int8_t bitCnt = 0;

uint16_t FallingEdge = 0;

//Control subsystem
gpState_t gamePadState = disconnected;
cmdType_t currentCmd;
cmdInitResponse_t initResponse;
cmdPollResponse_t pollResponse;

volatile uint16_t dump;
uint16_t timeout = 0;
/* -------------------------------- +
|	Prototype						|
+ ---------------------------------*/
void init(void);
void joyBusPoll(void);
void joyBusProbe(void);
void joyBusParse(void);

/* -------------------------------- +
|	Main							|
+ ---------------------------------*/
void main(void)
{
	/* -------------------------------- +
	|	Initialization					|
	+ ---------------------------------*/
	init();

	/* -------------------------------- +
	|	Main Loop						|
	+ ---------------------------------*/
	while (1)
	{
		switch (gamePadState)
		{
			//* -- Disconnected -- *//
			case disconnected:
			{
				joyBusProbe();
				break;
			}
			//* -- Probing ------- *//
			case probing:
			{
				__no_operation();
				break;
			}
			//* -- Connected ----- *//
			case connected:
			{
				DBGPRINT(BIT0);
				__delay_cycles(125000);
				DBGPRINT(0);
				joyBusPoll();
				break;
			}
			//* -- Polling ------- *//
			case polling:
			{
				__no_operation();
				break;
			}
			//* -- Answering ----- *//
			case answering:
			{
				do
				{
					while (JOYBUS_MASKED_PIN);	// Wait for the low state
					__no_operation();			// Wait to be in the middle of the bit period
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					__no_operation();
					bitCnt++;					//Count the bit
					*inputBuffPtr = JOYBUS_MASKED_PIN;
					inputBuffPtr++;
					while(!JOYBUS_MASKED_PIN);	//Wait for the high state
				} while (inputBuffPtr != inputLimitPtr);	//Loop until all bit has been received

				gamePadState = parsing;
				break;
			}
			case parsing:
			{
				//Parse the response
				joyBusParse();
				break;
			}
			//* -- Reporting ----- *//
			case reporting:
			{
				break;
			}
			default: gamePadState = disconnected;
			//* ------------------ *//
		}
	}
}

/* -------------------------------- +
|	Function						|
+ ---------------------------------*/
void init(void)
{
	WDT_A_hold(WDT_A_BASE);

	// == IO == //
	initPorts();
	// ======== //

	// == Clock == //
	if(initClocks(SYSTEM_CLOCK) != SYSTEM_CLOCK)
	{
		//Clock not set at the desired freq
		__no_operation();
	}
	// =========== //

	// == Timer == //
	// TIMER A0
	TA0CCTL0 = CCIE;							//Interrupt after each period
	TA0CCTL1 = TIMER_A_OUTPUTMODE_RESET_SET;	//Joybus Ch 1

	TA0CCR0 = __JOYBUS_BIT_PERIOD;				//Period for 1 bit
	TA0CTL =	TIMER_A_CLOCKSOURCE_SMCLK +
				TACLR;
	TA0EX0 = TIMER_A_CLOCKSOURCE_DIVIDER_1;

	// TIMER A1
	TA1CCTL1 = TIMER_A_OUTPUTMODE_SET_RESET;
	TA1CCR0 = __JOYBUS_BIT_PERIOD;
	TA1CCR1 = __JOYBUS_BIT_HIGH;
	TA1EX0 = TIMER_A_CLOCKSOURCE_DIVIDER_1;
	TA1CTL =	TIMER_A_CLOCKSOURCE_SMCLK +
				TIMER_A_UP_MODE +
				TACLR;

	// TIMER A2
	/*TA2CCTL2 =	TIMER_A_CAPTURECOMPARE_INTERRUPT_DISABLE +
				TIMER_A_CAPTUREMODE_RISING_AND_FALLING_EDGE +
				TIMER_A_CAPTURE_INPUTSELECT_CCIxA +
				TIMER_A_CAPTURE_SYNCHRONOUS +
				CAP;
	TA2EX0 = TIMER_A_CLOCKSOURCE_DIVIDER_1;
	TA2CTL =	TIMER_A_CLOCKSOURCE_SMCLK +
				TIMER_A_STOP_MODE +
				TACLR;*/
	// =========== //

	gamePadState = disconnected;

	__enable_interrupt();    // Enable interrupts globally
	__no_operation();
}

void joyBusProbe(void)
{
	uint8_t i=0;

	// -- Load the init message -- //
	TA0CCR1 = outputBuffInit[10];
	outputBuffPtr = &outputBuffInit[9];
	outputBuffNext = outputBuffInit[9];
	outputLimitPtr = &outputBuffInit[0];
	// --------------------------- //

	// -- Prepare the input buffer -- //
	for ( ; i<CMD_RESPONSE_SIZE_INIT+1; i++)
		inputBuffer[i] = 0;
	inputBuffPtr = &inputBuffer[0];
	inputLimitPtr = &inputBuffer[CMD_RESPONSE_SIZE_INIT+1];
	bitCnt = 0;
	currentCmd = cmdInit;
	// ------------------------------ //

	// -- Start the timer -- //
	gamePadState = probing;
	TA0CCTL1 = TIMER_A_OUTPUTMODE_RESET_SET;
	TA0CTL |= TIMER_A_UP_MODE;
	// --------------------- //
}

void joyBusPoll(void)
{
	uint8_t i=0;

	// -- Load the poll message -- //
	TA0CCR1 = outputBuffPoll[25];			//Load the first bit in the CCR
	outputBuffPtr = &outputBuffPoll[25];
	outputBuffNext = outputBuffPoll[25];
	outputLimitPtr = &outputBuffPoll[0];
	// --------------------------- //

	// -- Prepare the input buffer -- //
	for ( ; i<CMD_RESPONSE_SIZE_POLL+1; i++)
			inputBuffer[i] = 0;
	inputBuffPtr = &inputBuffer[0];
	inputLimitPtr = &inputBuffer[CMD_RESPONSE_SIZE_POLL+1];
	bitCnt = 0;
	currentCmd = cmdPoll;
	// ------------------------------ //

	// -- Start the timer -- //
	gamePadState = polling;
	TA0CCTL1 = TIMER_A_OUTPUTMODE_RESET_SET;
	TA0CTL |= TIMER_A_UP_MODE;
	// --------------------- //
}

void joyBusParse(void)
{
	int16_t i;
	uint64_t tempResponse = 0;
	uint64_t tempMask;

	// -- Recompile the response -- //
	bitCnt -= 2;								//Remove stop bit and inputLimitPtr handler
	tempMask = ((uint64_t)1)<<bitCnt;
	for (i=0; i<=bitCnt; i++)					//We received MSB first
	{
		if (inputBuffer[i] > 0)
			tempResponse |= tempMask;
		tempMask >>= 1;
	}
	// ---------------------------- //

	// -- Parse and act accordingly -- //
	switch (currentCmd)
	{
		case cmdInit:
		{
			initResponse.gamePadType = (uint16_t)(tempResponse >>8);
			initResponse.gamePadStatus = (uint8_t)tempResponse;

			if (initResponse.gamePadType == GAMEPAD_TYPE_GC)
				gamePadState = connected;
			else
				gamePadState = disconnected;
			break;
		}
		case cmdPoll:
		{
			pollResponse.all = tempResponse;

			if (pollResponse.btnB)
				P4OUT |= BIT7;
			else
				P4OUT &= ~BIT7;

			if (pollResponse.btnA)
				P1OUT |= BIT0;
			else
				P1OUT &= ~BIT0;

			gamePadState = connected;

			break;
		}
		case cmdRumble_on:
		{
			break;
		}
		case cmdRumble_off:
		{
			break;
		}
	}
	// ------------------------------- //
}

/* -------------------------------- +
|	ISR								|
+ ---------------------------------*/
// ======= TIMER_A0_CCR0 ======== //
#pragma vector = TIMER0_A0_VECTOR
__interrupt void TIMER0_A0_ISR(void)
{
	TA0CCR1 = outputBuffNext;

	outputBuffPtr--;

	if (outputBuffPtr == outputLimitPtr)
	{
		TA0CTL = 	TIMER_A_CLOCKSOURCE_SMCLK +
					TIMER_A_STOP_MODE +
					TACLR;
		TA0CCTL1 = TIMER_A_OUTPUTMODE_OUTBITVALUE_LOW;
		gamePadState = answering;
	}
	else
		outputBuffNext = *outputBuffPtr;

	//CCR0 auto clear flag when servicing the ISR
}

// ======= NMIs ====== //
#pragma vector = UNMI_VECTOR
__interrupt void USER_ISR(void)
{
	__no_operation();
}

#pragma vector = SYSNMI_VECTOR
__interrupt void SYSTEM_ISR(void)
{
	__no_operation();
}
